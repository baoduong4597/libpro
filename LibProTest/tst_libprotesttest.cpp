#include <QString>
#include "test_setup.h"
#include <QTextStream>
#include <QString>
#include "../LibProLib/Common/filehelper.h"
class LibProTestTest : public QObject
{
    Q_OBJECT

public:
    LibProTestTest();

private Q_SLOTS:
    void QStringListTest();
    void QTextStreamTest();
    void QStingSplitText();
    void WriteBooleanToFile();
};

LibProTestTest::LibProTestTest()
{

}


void LibProTestTest::QStringListTest()
{
    QStringList list;
    list.append("1");
    list.append("abd");
    QVERIFY(list.length()==2);
}

void LibProTestTest::QTextStreamTest()
{
    QString result;
    QTextStream st(&result);
    st << "_userID" << "|" <<"_email";
    qDebug().nospace()<<  result.toLatin1();
}

void LibProTestTest::QStingSplitText()
{
    QString data="1|duckhan123@gmail.com";

    QStringList values=data.split('|');

    int _userId=((QString)values.at(0)).toInt();
    QString _email =(QString)values.at(1);
    qDebug() << _userId << _email.toLatin1();
}

void LibProTestTest::WriteBooleanToFile()
{
    QStringList list;
    bool a=true;
    QString str;
    QTextStream stream(&str);
    stream << a;
    list.append(str);
    FileHelper::SaveAllLine("D:/testbool.txt",list);
}

#ifdef  LibProTestTestExcute
QTEST_APPLESS_MAIN(LibProTestTest);
#endif

#include "tst_libprotesttest.moc"
