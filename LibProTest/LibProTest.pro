#-------------------------------------------------
#
# Project created by QtCreator 2017-06-20T15:30:41
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_libprotesttest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        tst_libprotesttest.cpp \ 
    tst_filehelpertest.cpp \
    tst_usermanagertest.cpp \
    tst_bookmanagertest.cpp \
    tst_bookcarttest.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LibProLib/release/ -lLibProLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LibProLib/debug/ -lLibProLib
else:unix: LIBS += -L$$OUT_PWD/../LibProLib/ -lLibProLib

INCLUDEPATH += $$PWD/../LibProLib
DEPENDPATH += $$PWD/../LibProLib

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LibProLib/release/ -lLibProLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LibProLib/debug/ -lLibProLib
else:unix: LIBS += -L$$OUT_PWD/../LibProLib/ -lLibProLib

INCLUDEPATH += $$PWD/../LibProLib
DEPENDPATH += $$PWD/../LibProLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../LibProLib/release/libLibProLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../LibProLib/debug/libLibProLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../LibProLib/release/LibProLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../LibProLib/debug/LibProLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../LibProLib/libLibProLib.a

HEADERS += \
    test_setup.h
