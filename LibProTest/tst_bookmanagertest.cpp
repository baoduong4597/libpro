#include <QString>
#include "test_setup.h"
#include <QTextStream>
#include "../LibProLib/Manager/bookmanager.h"
class BookManagetTest : public QObject
{
    Q_OBJECT

public:
    BookManagetTest();

private Q_SLOTS:
    void loadBook();
};

BookManagetTest::BookManagetTest()
{

}

void BookManagetTest::loadBook()
{
    QList<Book*> list=BookManager::getInstance().Books();
    for (Book* b:list)
    {
        qDebug() << b->toString().toLocal8Bit();
    }
    QVERIFY(list.length()>0);
}


#ifdef  BookManagetTestExcute
QTEST_APPLESS_MAIN(BookManagetTest);
#endif

#include "tst_bookmanagertest.moc"
