#include "test_setup.h"
#include "../LibProLib/Manager/usermanager.h"
#include <QList>
class UserManagerTest : public QObject
{
    Q_OBJECT

public:
    UserManagerTest();
private:
    UserManager* manager;
private Q_SLOTS:
    void UserLoadedTest();
    void AccountLoadedTest();
    void RoleLoadedTest();
    void RoleMapLoadedTest();
    void LoginTest();
    void UserTest();
    void ModifyUserTest();
    void AddUserTest();
};



UserManagerTest::UserManagerTest()
{
}

void UserManagerTest::UserLoadedTest()
{
    QList<User*>& list =UserManager::Instance().Users();
    for(User* u :list)
    {
       qDebug() << u->toString().toLocal8Bit();
    }
    QVERIFY(list.count()>0);
}

void UserManagerTest::AccountLoadedTest()
{
    QList<Account*>& list =UserManager::Instance().Accounts();
    for(Account* u :list)
    {
       qDebug() << u->toString().toLocal8Bit();
    }
    QVERIFY(list.count()>0);

}

void UserManagerTest::RoleLoadedTest()
{
    QList<Role*>& list =UserManager::Instance().Roles();
    for(Role* u :list)
    {
       qDebug() << u->toString().toLocal8Bit();
    }
    QVERIFY(list.count()>0);

}

void UserManagerTest::RoleMapLoadedTest()
{
    QList<RoleMap*>& list =UserManager::Instance().RoleMaps();
    for(RoleMap* u :list)
    {
       qDebug() << u->toString().toLocal8Bit();
    }
    QVERIFY(list.count()>0);

}

void UserManagerTest::LoginTest()
{

}

void UserManagerTest::UserTest()
{
    User u=User(1,"1","NBame",QDateTime::currentDateTime(),"St","mail");
    QString str=u.toString();
    qDebug() << str.toLocal8Bit();
}

void UserManagerTest::ModifyUserTest()
{
    User* u=UserManager::Instance().getUserByID(1);
    User a=User(u->toString());
    a.setEmail("duckhan123@gmai.com");
    UserManager::Instance().ModifyUser(a);
    u=UserManager::Instance().getUserByID(1);
    QVERIFY(u->getEmail()==a.getEmail());
}

void UserManagerTest::AddUserTest()
{
    QList<User*> current=UserManager::Instance().Users();
    int curCount=current.length();
    User  u=User(0,"1512492","Bui thi truc ly",QDateTime::currentDateTime(),"Student","trucly@gmai.com");
    bool ret=UserManager::Instance().AddUser(u);
    ret&=UserManager::Instance().SaveToFile();
    int newCount=UserManager::Instance().Users().length();
    qDebug() << u.toString().toLocal8Bit();
    QVERIFY(ret);
    QVERIFY(curCount==newCount-1);
}
#ifdef  UserManagerTestExcute
QTEST_APPLESS_MAIN(UserManagerTest);
#endif

#include "tst_usermanagertest.moc"
