#include <QString>
#include "test_setup.h"
#include <QTextStream>
#include "../LibProLib/Manager/sessionmanager.h"
#include "../LibProLib/Manager/bookmanager.h"
class BookCartTest : public QObject
{
    Q_OBJECT

public:
    BookCartTest();

private Q_SLOTS:
    void addBook();
};

BookCartTest::BookCartTest()
{

}

void BookCartTest::addBook()
{
    Book* b=BookManager::getInstance().Books().first();
    SessionManager::Current().getBookCart()->addBook(b);
    b=new Book(5,"Tets Book","desction book","Duc Khan","NXB DK",5,1);
    SessionManager::Current().getBookCart()->addBook(b);
    QList<Book*> books=SessionManager::Current().getBookCart()->getAllBook();
    for(Book* book:books)
    {
        qDebug()  << book->toString().toLocal8Bit();
    }
    QVERIFY(books.length()==2);
}



#ifdef  BookCartTestExcute
QTEST_APPLESS_MAIN(BookCartTest);
#endif

#include "tst_bookcarttest.moc"
