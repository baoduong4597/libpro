#ifndef CHANGE_PASS_H
#define CHANGE_PASS_H

#include <QWidget>

namespace Ui {
class Change_pass;
}

class Change_pass : public QWidget
{
    Q_OBJECT

public:
    explicit Change_pass(QWidget *parent = 0);
    ~Change_pass();

private slots:
    void on_BackButton_clicked();

    void on_ChangeButton_clicked();

private:
    Ui::Change_pass *ui;
};

#endif // CHANGE_PASS_H
