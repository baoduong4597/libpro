#ifndef MODIFY_ACCOUNT_H
#define MODIFY_ACCOUNT_H

#include <QWidget>
#include "../LibProLib/Manager/usermanager.h"
namespace Ui {
class Modify_account;
}

class Modify_account : public QWidget
{
    Q_OBJECT

public:
    explicit Modify_account(QWidget *parent = 0,QObject* param= nullptr);
    ~Modify_account();

private slots:
    void on_ApplyButton_clicked();

private:
    Ui::Modify_account *ui;
    Account* acc;
    RoleMap* role;
};

#endif // MODIFY_ACCOUNT_H
