#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "navigationservice.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    NavigationService::init(ui->widget);
    NavigationService::getInstance().navigate(eWidget::WMain_menu);
}

MainWindow::~MainWindow()
{
    delete ui;
}
