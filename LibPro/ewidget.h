#ifndef EWIDGET_H
#define EWIDGET_H
enum eWidget{
    WAdd_account,
    WAdd_book,
    WAdd_user,
    WAdmin_profile,
    Wborrowed_book,
    WChange_pass,
    WDelete_account,
    WDelete_book,
    WHistory,
    WLibra_account,
    WLock_acount,
    WLogin,
    WMain_menu,
    WModify_account,
    WModify_book,
    WModify_user,
    WNotice,
    WPunish,
    WSearch,
    WSearch_account,
    WSearch_user,
    WSearch_book,
    WTaken_book,
    WUnlock_account,
    WUser_profile,
    WBook_cart,
    WBorrowedBook_View
};

#endif // EWIDGET_H
