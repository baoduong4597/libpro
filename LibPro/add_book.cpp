#include "add_book.h"
#include "ui_add_book.h"
#include "../LibProLib/Manager/bookmanager.h"
#include <QMessageBox>
#include "navigationservice.h"
Add_book::Add_book(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Add_book)
{
    ui->setupUi(this);
}

Add_book::~Add_book()
{
    delete ui;
}

void Add_book::on_AddButton_clicked()
{
    QString title=ui->TittlelineEdit->text();
    QString author=ui->AuthorlineEdit->text();
    QString publisher=ui->PublisherlineEdit->text();
    int quantity=ui->QuantityBox->value();
    QString description=ui->DescriptionlineEdit->text();
    Book b(0,title,description,author,publisher,quantity,0);
    BookManager::getInstance().addBook(b);
    if(b.getBookID()!=0)
    {
        QMessageBox::information(this,"Thông báo","Thêm thành công");
        NavigationService::getInstance().goBack();
    }
    else
    {
        QMessageBox::warning(this,"Thông báo","Thêm thất bại");
    }
}

void Add_book::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}
