#include "notice.h"
#include "ui_notice.h"
#include <QMessageBox>
#include "../LibProLib/Manager/messagemanager.h"
#include "../LibProLib/Manager/sessionmanager.h"
#include "../LibProLib/Manager/usermanager.h"
#include <QStandardItemModel>
#include "navigationservice.h"
Notice::Notice(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Notice)
{
    ui->setupUi(this);
}

Notice::~Notice()
{
    delete ui;
}

void Notice::showEvent(QShowEvent *event)
{
    int userid=SessionManager::Current().getAccount()->getUserID();
    QList<Message*> msgs=MessageManager::getInstance().getMessageByUser(userid);
    QStandardItemModel* model=new QStandardItemModel(msgs.length(),4);
    model->setHorizontalHeaderItem(0,new QStandardItem("Message Id"));
    model->setHorizontalHeaderItem(1,new QStandardItem("Sender"));
    model->setHorizontalHeaderItem(2,new QStandardItem("Sent Date"));
    model->setHorizontalHeaderItem(3,new QStandardItem("Content"));
    QStandardItem* item;

    for(int i=0;i<msgs.length();i++)
    {
        Message* m=msgs.at(i);
        User* u=UserManager::Instance().getUserByID(m->getSendID());

        item=new QStandardItem(QString::number(m->getMessId()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,0,item);


        item=new QStandardItem(u->getFullName());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,1,item);


        item=new QStandardItem(m->getSentDate().toString(Qt::ISODate));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,2,item);


        item=new QStandardItem(m->getContent());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,3,item);


    }
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(3,QHeaderView::Stretch);
}

void Notice::on_BackButton_clicked()
{
        NavigationService::getInstance().goBack();
}
