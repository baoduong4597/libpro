#ifndef LOCK_ACCOUNT_H
#define LOCK_ACCOUNT_H

#include <QWidget>

namespace Ui {
class Lock_account;
}

class Lock_account : public QWidget
{
    Q_OBJECT

public:
    explicit Lock_account(QWidget *parent = 0);
    ~Lock_account();

private:
    Ui::Lock_account *ui;
};

#endif // LOCK_ACCOUNT_H
