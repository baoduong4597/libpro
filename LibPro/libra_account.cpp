#include "libra_account.h"
#include "ui_libra_account.h"
#include "navigationservice.h"
Libra_account::Libra_account(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Libra_account)
{
    ui->setupUi(this);
}

Libra_account::~Libra_account()
{
    delete ui;
}

void Libra_account::on_SearchButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WSearch_book);
}

void Libra_account::on_ModifyButton_clicked()
{
     NavigationService::getInstance().navigate(eWidget::WModify_book);
}

void Libra_account::on_AddButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WAdd_book);
}

void Libra_account::on_LogoutButton_clicked()
{
    SessionManager::Current().logout();
    NavigationService::getInstance().goHome();
}

void Libra_account::on_pushButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WBorrowedBook_View);
}
