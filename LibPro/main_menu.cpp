#include "main_menu.h"
#include "ui_main_menu.h"
#include "navigationservice.h"
Main_menu::Main_menu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Main_menu)
{
    ui->setupUi(this);
}

Main_menu::~Main_menu()
{
    delete ui;
}

void Main_menu::on_QuitButton_clicked()
{
   QApplication::quit();
}

void Main_menu::on_LoginButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WLogin);
}

void Main_menu::on_SearchButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WSearch_book);
}
