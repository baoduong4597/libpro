#include "navigationservice.h"
#include "admin_profile.h"
#include "user_profile.h"
#include "libra_account.h"
#include "search_book.h"
#include "notice.h"
#include "modify_book.h"
#include "modify_account.h"
#include "search_account.h"
#include "search_user.h"
#include "add_account.h"
#include "add_book.h"
#include "book_cart.h"
#include "history.h"
#include "borrowedbook_view.h"
#include "add_user.h"
#include "change_pass.h"
NavigationService NavigationService::_instance=NavigationService();
NavigationService::NavigationService()
{
    _current=nullptr;
}

void NavigationService::init(QWidget *mainFrame)
{
    _instance._mainFrame=mainFrame;
}

NavigationService &NavigationService::getInstance()
{
    if(_instance._mainFrame==nullptr)
    {
        throw "run init before use it";
    }
    return _instance;
}

void NavigationService::navigate(eWidget e,QObject* param)
{
    if(_current!=nullptr)
    {
        _current->hide();
        _frames.append(_current);
    }
    switch (e)
    {
    case eWidget::WNotice:
    {
        _current=new Notice(_mainFrame);
        break;
    }
    case eWidget::WMain_menu:
    {
        _current=new Main_menu(_mainFrame);
        break;
    }
    case eWidget::WAdd_user:
    {
        _current=new Add_User(_mainFrame);
        break;
    }
    case eWidget::WChange_pass:
    {
        _current=new Change_pass(_mainFrame);
        break;
    }
    case eWidget::WModify_account:
    {
        _current=new Modify_account(_mainFrame,param);
        break;
    }
    case eWidget::WLogin:
    {
        _current=new Login(_mainFrame);
        break;
    }
    case eWidget::WSearch_book:
    {
        _current=new Search_book(_mainFrame);
        break;
    }
    case eWidget::WAdmin_profile:
    {

        _current=new Admin_profile(_mainFrame);
        break;

    }
    case eWidget::WUser_profile:
    {
        _current=new User_profile(_mainFrame);
        break;
    }
    case eWidget::WLibra_account:
    {
        _current=new Libra_account(_mainFrame);
        break;
    }
    case eWidget::WAdd_account:
    {
        _current=new Add_account(_mainFrame);
        break;
    }
    case eWidget::WAdd_book:
    {
        _current=new Add_book(_mainFrame);
        break;
    }
    case eWidget::WModify_book:
    {
        _current=new Modify_book(_mainFrame,param);
        break;
    }
    case eWidget::WSearch_account:
    {
        _current=new Search_account(_mainFrame);
        break;
    }
    case eWidget::WSearch_user:
    {
        _current=new Search_user(_mainFrame);
        break;
    }
    case eWidget::WBook_cart:
    {
        _current=new Book_cart(_mainFrame);
        break;
    }
    case eWidget::WHistory:
    {
        _current=new History(_mainFrame);
        break;
    }
    case eWidget::WBorrowedBook_View:
    {
        _current=new BorrowedBook_View(_mainFrame);
        break;
    }
    default:
        _current=new Main_menu(_mainFrame);
        break;
    }
    _current->show();
}

void NavigationService::goBack()
{
    _current->hide();
    _waitDelete.append(_current);
    _current= _frames.pop();
    _current->show();

}

void NavigationService::goHome()
{
    _frames.clear();
    navigate(eWidget::WMain_menu);
}


