#ifndef SEARCH_USER_H
#define SEARCH_USER_H

#include <QWidget>

namespace Ui {
class Search_user;
}

class Search_user : public QWidget
{
    Q_OBJECT

public:
    explicit Search_user(QWidget *parent = 0);
    ~Search_user();
protected:
    void showEvent(QShowEvent * event) override;
private slots:
    void on_SearchlineEdit_returnPressed();

    void on_DeleteButton_clicked();

    void on_BackButton_clicked();

    void on_ModifyUserButton_clicked();

private:
    Ui::Search_user *ui;
    void refresh();
};

#endif // SEARCH_USER_H
