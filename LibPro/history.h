#ifndef HISTORY_H
#define HISTORY_H

#include <QWidget>

namespace Ui {
class History;
}

class History : public QWidget
{
    Q_OBJECT

public:
    explicit History(QWidget *parent = 0);
    ~History();
protected:
    void showEvent(QShowEvent * event) override;
private slots:
    void on_BackButton_clicked();

private:
    Ui::History *ui;
};

#endif // HISTORY_H
