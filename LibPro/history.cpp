#include "history.h"
#include "ui_history.h"
#include "../LibProLib/Manager/bookmanager.h"
#include "../LibProLib/Manager/sessionmanager.h"
#include "navigationservice.h"
#include <QStandardItemModel>
History::History(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::History)
{
    ui->setupUi(this);
}

History::~History()
{
    delete ui;
}

void History::showEvent(QShowEvent *event)
{
    int uid=SessionManager::Current().getAccount()->getUserID();
    QList<BorrowedBook*> books=BookManager::getInstance().getAllBorrowedBook(uid);
    QStandardItemModel* model=new QStandardItemModel(books.length(),7);
    model->setHorizontalHeaderItem(0,new QStandardItem("Book Id"));
    model->setHorizontalHeaderItem(1,new QStandardItem("Title"));
    model->setHorizontalHeaderItem(2,new QStandardItem("Author"));
    model->setHorizontalHeaderItem(3,new QStandardItem("Publisher"));
     model->setHorizontalHeaderItem(4,new QStandardItem("Borrowed Date"));
    model->setHorizontalHeaderItem(5,new QStandardItem("Expiry Date"));
    model->setHorizontalHeaderItem(6,new QStandardItem("Status"));
    QStandardItem* item;

    for(int i=0;i<books.length();i++)
    {
        BorrowedBook* bo=books.at(i);
        Book* b=BookManager::getInstance().getBookById(bo->getBookID());
        item=new QStandardItem(QString::number(bo->getBookID()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,0,item);


        item=new QStandardItem(b->getTitle());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,1,item);


        item=new QStandardItem(b->getAuthor());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,2,item);


        item=new QStandardItem(b->getPublisher());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,3,item);

        item=new QStandardItem(bo->getBorrowedDate().toString(Qt::ISODate));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,4,item);

        item=new QStandardItem(bo->getExpiryDate().toString(Qt::ISODate));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,5,item);
        QString status=bo->getStatus()?"Đã trả":"Chưa trả";
        item=new QStandardItem(status);
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,6,item);

    }
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(6,QHeaderView::Stretch);
}

void History::on_BackButton_clicked()
{
NavigationService::getInstance().goBack();
}
