#ifndef BOOK_CART_H
#define BOOK_CART_H

#include <QWidget>

namespace Ui {
class Book_cart;
}

class Book_cart : public QWidget
{
    Q_OBJECT

public:
    explicit Book_cart(QWidget *parent = 0);
    ~Book_cart();
protected:
    void showEvent(QShowEvent * event) override;
private slots:
    void on_ConfirmButton_clicked();
    void refresh();
    void on_BackButton_clicked();

    void on_RemoveButton_clicked();

private:
    Ui::Book_cart *ui;
};

#endif // BOOK_CART_H
