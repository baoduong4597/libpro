#ifndef BORROWEDBOOK_VIEW_H
#define BORROWEDBOOK_VIEW_H

#include <QWidget>

namespace Ui {
class BorrowedBook_View;
}

class BorrowedBook_View : public QWidget
{
    Q_OBJECT

public:
    explicit BorrowedBook_View(QWidget *parent = 0);
    ~BorrowedBook_View();
protected:
    void showEvent(QShowEvent * event) override;
private slots:
    void on_ExpiredcheckBox_stateChanged(int arg1);

    void on_TitlelineEdit_returnPressed();

    void on_BackButton_clicked();

    void on_BorrowedBook_View_destroyed();

    void on_PunishButton_clicked();

private:
    Ui::BorrowedBook_View *ui;
    void refresh();
};

#endif // BORROWEDBOOK_VIEW_H
