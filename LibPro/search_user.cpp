#include "search_user.h"
#include "ui_search_user.h"
#include "../LibProLib/Manager/usermanager.h"
#include <QStandardItemModel>
#include <QMessageBox>
#include "navigationservice.h"
Search_user::Search_user(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Search_user)
{
    ui->setupUi(this);
}

Search_user::~Search_user()
{
    delete ui;
}

void Search_user::showEvent(QShowEvent *event)
{
    refresh();
}

void Search_user::on_SearchlineEdit_returnPressed()
{
    refresh();
}

void Search_user::refresh()
{
    QString name=ui->SearchlineEdit->text();
    QList<User*> users=UserManager::Instance().getAllUserByName(name);
    QStandardItemModel* model=new QStandardItemModel(users.length(),6);
    model->setHorizontalHeaderItem(0,new QStandardItem("UserId"));
    model->setHorizontalHeaderItem(1,new QStandardItem("Identify Number"));
    model->setHorizontalHeaderItem(2,new QStandardItem("Full Name"));
    model->setHorizontalHeaderItem(3,new QStandardItem("Birthday"));
    model->setHorizontalHeaderItem(4,new QStandardItem("Job"));
    model->setHorizontalHeaderItem(5,new QStandardItem("Email"));
    QStandardItem* item;

    for(int i=0;i<users.length();i++)
    {
        User* u=users.at(i);
        item=new QStandardItem(QString::number(u->getUserId()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,0,item);


        item=new QStandardItem(u->getIdentifyNumber());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,1,item);


        item=new QStandardItem(u->getFullName());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,2,item);


        item=new QStandardItem(u->getBirthday().toString(Qt::ISODate));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,3,item);



        item=new QStandardItem(u->getJob());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,4,item);

        item=new QStandardItem(u->getEmail());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,5,item);

    }
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(5,QHeaderView::Stretch);
}

void Search_user::on_DeleteButton_clicked()
{
    int uid=ui->tableView->model()->index(ui->tableView->currentIndex().row(),0).data().toInt();
    User* u=UserManager::Instance().getUserByID(uid);
    if(u)
    {
        QString msg="Bạn có chắc chắn muốn xóa User có Id "+QString::number(uid)+ " không?";
        if(u && QMessageBox::information(this,"Xác nhận",msg,QMessageBox::Yes,QMessageBox::No)==(int)QMessageBox::Yes)
        {
            if(UserManager::Instance().Remove(uid))
            {
                QMessageBox::information(this,"Thông tin","Xóa thành công");
                refresh();
            }
        }
    }
    else
    {
        QMessageBox::warning(this,"Lỗi","Đã xảy ra lỗi. Xin thử lại sau.");
    }
}

void Search_user::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}

void Search_user::on_ModifyUserButton_clicked()
{
    int uid=ui->tableView->model()->index(ui->tableView->currentIndex().row(),0).data().toInt();
    User* u=UserManager::Instance().getUserByID(uid);
    // NavigationService::getInstance().navigate(eWidget::W);
}
