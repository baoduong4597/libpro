#include "admin_profile.h"
#include "ui_admin_profile.h"
#include "navigationservice.h"
#include "../LibProLib/Manager/sessionmanager.h"
Admin_profile::Admin_profile(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Admin_profile)
{
    ui->setupUi(this);
}

Admin_profile::~Admin_profile()
{
    delete ui;
}

void Admin_profile::on_AddButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WAdd_account);
}

void Admin_profile::on_ModifyButton_clicked()
{
     NavigationService::getInstance().navigate(eWidget::WModify_account);
}

void Admin_profile::on_SearchButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WSearch_account);
}

void Admin_profile::on_SearchUserButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WSearch_user);

}

void Admin_profile::on_LogoutButton_clicked()
{
    SessionManager::Current().logout();
    NavigationService::getInstance().goHome();
}

void Admin_profile::on_AddUserButton_clicked()
{
     NavigationService::getInstance().navigate(eWidget::WAdd_user);
}
