#include "login.h"
#include "ui_login.h"
#include "navigationservice.h"
#include <QMessageBox>
#include "../LibProLib/Manager/sessionmanager.h"
#include "../LibProLib/Manager/usermanager.h"
Login::Login(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
}

Login::~Login()
{
    delete ui;
}

void Login::on_LoginButton_clicked()
{
    QString user=ui->UsernamelineEdit->text();
    QString pass=ui->PasswordlineEdit->text();
    UserManager& manager= UserManager::Instance();
    int result=manager.login(user,pass);
    if(result==0)
    {
        QMessageBox::information(this,"Info","Đăng nhập thành công");
        Account* acc=UserManager::Instance().getAccountByName(user);
        RoleMap* role=UserManager::Instance().getRoleMapByAccountId(acc->getAccountID());
        SessionManager::Current().setAccount(acc);
         SessionManager::Current().setRoleMap(role);
        switch(role->getRoleID())
        {
        case 1://admin
        {
            NavigationService::getInstance().navigate(eWidget::WAdmin_profile);
            break;
        }
        case 2://librarian
        {
            NavigationService::getInstance().navigate(eWidget::WLibra_account);
            break;
        }
        case 3://user
        {
            NavigationService::getInstance().navigate(eWidget::WUser_profile);
            break;
        }
        }

    }
    else if(result==3)
    {
        QMessageBox::warning(this,"Info","Tài khoản bị khóa");
    }
    else
    {
        QMessageBox::warning(this,"Info","Sai tên hoặc mật khấu");
    }
}

void Login::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}


