#ifndef ADD_USER_H
#define ADD_USER_H

#include <QWidget>

namespace Ui {
class Add_User;
}

class Add_User : public QWidget
{
    Q_OBJECT

public:
    explicit Add_User(QWidget *parent = 0);
    ~Add_User();

private slots:
    void on_BackButton_clicked();

    void on_AddButton_clicked();

private:
    Ui::Add_User *ui;
};

#endif // ADD_USER_H
