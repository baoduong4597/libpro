#include "borrowedbook_view.h"
#include "ui_borrowedbook_view.h"
#include "../LibProLib/Manager/bookmanager.h"
#include "../LibProLib/Manager/messagemanager.h"
#include "navigationservice.h"
#include <QMessageBox>
#include <QStandardItemModel>
#include "../LibProLib/Manager/sessionmanager.h"
BorrowedBook_View::BorrowedBook_View(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BorrowedBook_View)
{
    ui->setupUi(this);
}

BorrowedBook_View::~BorrowedBook_View()
{
    delete ui;
}

void BorrowedBook_View::showEvent(QShowEvent *event)
{
    refresh();
}

void BorrowedBook_View::refresh()
{
    QString title=ui->TitlelineEdit->text();
    QList<BorrowedBook*> books;
    if(ui->ExpiredcheckBox->isChecked())
    {
        books=BookManager::getInstance().getExpiredBooks();
    }
    else
    {
         books=BookManager::getInstance().getAllBorrowedBookByTitle(title);
    }
    QStandardItemModel* model=new QStandardItemModel(books.length(),7);
    model->setHorizontalHeaderItem(0,new QStandardItem("Book Id"));
    model->setHorizontalHeaderItem(1,new QStandardItem("Title"));
    model->setHorizontalHeaderItem(2,new QStandardItem("Author"));
    model->setHorizontalHeaderItem(3,new QStandardItem("Publisher"));
    model->setHorizontalHeaderItem(4,new QStandardItem("Borrowed Date"));
    model->setHorizontalHeaderItem(5,new QStandardItem("Expiry Date"));
    model->setHorizontalHeaderItem(6,new QStandardItem("Status"));
    QStandardItem* item;

    for(int i=0;i<books.length();i++)
    {
        BorrowedBook* bo=books.at(i);
        Book* b=BookManager::getInstance().getBookById(bo->getBookID());
        item=new QStandardItem(QString::number(bo->getBookID()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,0,item);


        item=new QStandardItem(b->getTitle());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,1,item);


        item=new QStandardItem(b->getAuthor());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,2,item);


        item=new QStandardItem(b->getPublisher());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,3,item);

        item=new QStandardItem(bo->getBorrowedDate().toString(Qt::ISODate));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,4,item);

        item=new QStandardItem(bo->getExpiryDate().toString(Qt::ISODate));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,5,item);
        QString status=bo->getStatus()?"Đã trả":"Chưa trả";
        item=new QStandardItem(status);
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,6,item);

    }
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(6,QHeaderView::Stretch);
}

void BorrowedBook_View::on_ExpiredcheckBox_stateChanged(int arg1)
{
    if(ui->ExpiredcheckBox->isChecked())
        refresh();
}

void BorrowedBook_View::on_TitlelineEdit_returnPressed()
{
    ui->ExpiredcheckBox->setChecked(false);
    refresh();

}

void BorrowedBook_View::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}

void BorrowedBook_View::on_BorrowedBook_View_destroyed()
{

}

void BorrowedBook_View::on_PunishButton_clicked()
{
    QModelIndex index=ui->tableView->currentIndex();
    int borrowedId=ui->tableView->model()->index(index.row(),0).data().toInt();
    if(borrowedId>0)
    {
        BorrowedBook* b=BookManager::getInstance().getBorrowedBookById(borrowedId);
        Message m(0,SessionManager::Current().getAccount()->getUserID(),b->getUserID(),"Bạn đã mượn sách quá hạn, đề nghị bạn mau chóng mạng sách đến trả kèm tiền phạt");
        MessageManager::getInstance().addMessage(m);
        QMessageBox::information(this,"Thông tin",m.toString());
    }
    else
    {
        QMessageBox::warning(this,"Lỗi","Đã xảy lỗi, xin thử lại.");
    }
}
