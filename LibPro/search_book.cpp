#include "search_book.h"
#include "ui_search_book.h"
#include <QMessageBox>
#include "navigationservice.h"
#include "../LibProLib/Manager/bookmanager.h"
#include <QStandardItemModel>
#include "../LibProLib/Manager/sessionmanager.h"
Search_book::Search_book(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Search_book)
{
    ui->setupUi(this);
}

Search_book::~Search_book()
{
    delete ui;
}

void Search_book::showEvent(QShowEvent *event)
{
    if(SessionManager::Current().getRoleMap()==nullptr || SessionManager::Current().getRoleMap()->getRoleID() !=2)
    {
        ui->DeleteButton->setVisible(false);
        ui->ModifyButton->setVisible(false);
        ui->AddButton->setVisible(true);
    }
    else
    {
        ui->ModifyButton->setVisible(true);
        ui->DeleteButton->setVisible(true);
        ui->AddButton->setVisible(false);
    }
    refresh();
}

void Search_book::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}

void Search_book::refresh()
{
    QString title=ui->SearchlineEdit->text();
    QList<Book*> books=BookManager::getInstance().getBooksByTitle(title);
    QStandardItemModel* model=new QStandardItemModel(books.length(),6);
    model->setHorizontalHeaderItem(0,new QStandardItem("Book Id"));
    model->setHorizontalHeaderItem(1,new QStandardItem("Title"));
    model->setHorizontalHeaderItem(2,new QStandardItem("Author"));
    model->setHorizontalHeaderItem(3,new QStandardItem("Publisher"));
    model->setHorizontalHeaderItem(4,new QStandardItem("Available"));
    model->setHorizontalHeaderItem(5,new QStandardItem("Description"));
    QStandardItem* item;

    for(int i=0;i<books.length();i++)
    {
        Book* b=books.at(i);
        item=new QStandardItem(QString::number(b->getBookID()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,0,item);


        item=new QStandardItem(b->getTitle());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,1,item);


        item=new QStandardItem(b->getAuthor());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,2,item);


        item=new QStandardItem(b->getPublisher());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,3,item);



        item=new QStandardItem(QString::number(b->getAvailable()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,4,item);

        item=new QStandardItem(b->getDecription());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);

        model->setItem(i,5,item);

    }
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(5,QHeaderView::Stretch);
}

void Search_book::on_SearchlineEdit_returnPressed()
{
    refresh();
}

void Search_book::on_AddButton_clicked()
{
    QModelIndex index=ui->tableView->currentIndex();
    int bookId=ui->tableView->model()->data(ui->tableView->model()->index(index.row(),0)).toInt();
    int count=ui->tableView->model()->index(index.row(),4).data().toInt();
    if(count==0)
    {
        QMessageBox::information(this,"Thông báo","Số lượng sách trong kho không đủ, vui long quay lại sau.");
    }
    Book* b=BookManager::getInstance().getBookById(bookId);
    if(b)
    {
        QString msg="Bạn có muốn thêm quyển sách " +b->getTitle()+" vào giỏ?";
        QMessageBox::StandardButton result=(QMessageBox::StandardButton)QMessageBox::question(this,"Xác nhận",msg,QMessageBox::Yes,QMessageBox::No);
        if(result==QMessageBox::Yes)
        {
            SessionManager::Current().getBookCart()->addBook(b);
            QMessageBox::information(this,"Thông báo","Thêm thành công");
            refresh();
        }
    }
    else
    {
        QMessageBox::warning(this,"Lỗi","Đã xảy ra lỗi, vui lòng thử lại");
    }
}


void Search_book::on_DeleteButton_clicked()
{
    QModelIndex index=ui->tableView->currentIndex();
    int bookId=ui->tableView->model()->data(ui->tableView->model()->index(index.row(),0)).toInt();
    Book* b=BookManager::getInstance().getBookById(bookId);
    if(b)
    {
        QString msg="Bạn có xóa quyển sách " +b->getTitle()+" không?";
        QMessageBox::StandardButton result=(QMessageBox::StandardButton)QMessageBox::question(this,"Xác nhận",msg,QMessageBox::Yes,QMessageBox::No);
        if(result==QMessageBox::Yes)
        {
            BookManager::getInstance().removeBook(b->getBookID());
             QMessageBox::information(this,"Thông báo","Xóa thành công");
             refresh();
        }
    }
    else
    {
        QMessageBox::warning(this,"Lỗi","Đã xảy ra lỗi, vui lòng thử lại");
    }
}

void Search_book::on_ModifyButton_clicked()
{
    QModelIndex index=ui->tableView->currentIndex();
    int bookId=ui->tableView->model()->data(ui->tableView->model()->index(index.row(),0)).toInt();
    Book* b=BookManager::getInstance().getBookById(bookId);
    NavigationService::getInstance().navigate(eWidget::WModify_book,b);
}
