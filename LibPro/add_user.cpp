#include "add_user.h"
#include "ui_add_user.h"
#include "navigationservice.h"
#include "../LibProLib/Manager/usermanager.h"
#include <QMessageBox>
Add_User::Add_User(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Add_User)
{
    ui->setupUi(this);
}

Add_User::~Add_User()
{
    delete ui;
}

void Add_User::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}

void Add_User::on_AddButton_clicked()
{
    User u;
    u.setFullName(ui->FullNameText->text());
    u.setEmail(ui->EmailText->text());
    u.setBirthday(ui->BirthdayBox->dateTime());
    u.setIdentifyNumber(ui->IdentofyNumberText->text());
    u.setJob(ui->JobText->text());
    if(UserManager::Instance().AddUser(u))
    {
        QMessageBox::information(this,"Thông báo","Thêm user thành công.");
        NavigationService::getInstance().goBack();
    }
    else
    {
        QMessageBox::warning(this,"Lỗi","Vui lòng thử lại");
    }
}
