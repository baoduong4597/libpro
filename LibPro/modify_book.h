#ifndef MODIFY_BOOK_H
#define MODIFY_BOOK_H

#include <QWidget>
#include "../LibProLib/Models/book.h"
namespace Ui {
class Modify_book;
}

class Modify_book : public QWidget
{
    Q_OBJECT

public:
    explicit Modify_book(QWidget *parent = 0,QObject* param=nullptr);
    ~Modify_book();

private slots:
    void on_ApplyButton_clicked();

    void on_BackButton_clicked();

private:
    Ui::Modify_book *ui;
    Book* book;
};

#endif // MODIFY_BOOK_H
