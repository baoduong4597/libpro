#include "search_account.h"
#include "ui_search_account.h"
#include "navigationservice.h"
#include <QStandardItemModel>
#include <QMessageBox>
Search_account::Search_account(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Search_account)
{
    ui->setupUi(this);
}

Search_account::~Search_account()
{
    delete ui;
}

void Search_account::showEvent(QShowEvent *event)
{
    refresh();

}

void Search_account::on_pushButton_clicked()
{
    NavigationService::getInstance().goBack();
}

void Search_account::refresh()
{
    QString name=ui->SearchlineEdit->text();
    QList<Account*> accounts=UserManager::Instance().getAllAccountByAccName(name);
    QStandardItemModel* model=new QStandardItemModel(accounts.length(),7);
    model->setHorizontalHeaderItem(0,new QStandardItem("Account Id"));
    model->setHorizontalHeaderItem(1,new QStandardItem("User Id"));
    model->setHorizontalHeaderItem(2,new QStandardItem("Full Name"));
    model->setHorizontalHeaderItem(3,new QStandardItem("UserName"));
    model->setHorizontalHeaderItem(4,new QStandardItem("Password"));
    model->setHorizontalHeaderItem(5,new QStandardItem("Role"));
    model->setHorizontalHeaderItem(6,new QStandardItem("Status"));
    QStandardItem* item;

    for(int i=0;i<accounts.length();i++)
    {
        Account* acc=accounts.at(i);
        User* u=UserManager::Instance().getUserByID(acc->getUserID());
        item=new QStandardItem(QString::number(acc->getAccountID()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,0,item);


        item=new QStandardItem(QString::number(acc->getUserID()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,1,item);


        item=new QStandardItem(u->getFullName());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,2,item);


        item=new QStandardItem(acc->getAccountName());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,3,item);

        QString str(acc->getPassword().length(),'*');
        item=new QStandardItem(str);
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,4,item);

        RoleMap* map=UserManager::Instance().getRoleMapByAccountId(acc->getAccountID());
        Role* role=UserManager::Instance().getRoleById(map->getRoleID());
        item=new QStandardItem(role->getRoleDecription());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,5,item);


        item=new QStandardItem(acc->getStatus()?"Hoạt động":"Khóa");
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,6,item);

    }
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(6,QHeaderView::Stretch);
}

void Search_account::on_ModifyButton_clicked()
{

    int accId=ui->tableView->model()->index(ui->tableView->currentIndex().row(),0).data().toInt();
    Account* acc=UserManager::Instance().getAccountByAccountId(accId);
    if(acc)
    {
        NavigationService::getInstance().navigate(eWidget::WModify_account,acc);
    }

    else
    {
        QMessageBox::warning(this,"Lỗi","Đã xảy ra lỗi, thử lại sau.");
    }
}

void Search_account::on_DeleteButton_clicked()
{
    int accId=ui->tableView->model()->index(ui->tableView->currentIndex().row(),0).data().toInt();
    Account* acc=UserManager::Instance().getAccountByAccountId(accId);
    if(acc)
    {
        QString msg="Bạn có chắc chắn muốn xóa account "+acc->getAccountName() + " không?";
       if(QMessageBox::question(this,"Xác nhận",msg,QMessageBox::Yes,QMessageBox::No)==(int) QMessageBox::Yes)
       {
            UserManager::Instance().RemoveAccount(accId);
            refresh();
       }
    }

    else
    {
        QMessageBox::warning(this,"Lỗi","Đã xảy ra lỗi, thử lại sau.");
    }
}

void Search_account::on_SearchlineEdit_returnPressed()
{
    refresh();
}
