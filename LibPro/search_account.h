#ifndef SEARCH_ACCOUNT_H
#define SEARCH_ACCOUNT_H

#include <QWidget>

namespace Ui {
class Search_account;
}

class Search_account : public QWidget
{
    Q_OBJECT

public:
    explicit Search_account(QWidget *parent = 0);
    ~Search_account();
protected:
    void showEvent(QShowEvent * event) override;
private slots:
    void on_pushButton_clicked();

    void on_ModifyButton_clicked();

    void on_DeleteButton_clicked();

    void on_SearchlineEdit_returnPressed();

private:
    Ui::Search_account *ui;
    void refresh();
};

#endif // SEARCH_ACCOUNT_H
