#ifndef NAVIGATIONSERVICE_H
#define NAVIGATIONSERVICE_H
#include <QWidget>
#include <QStack>
#include "ewidget.h"
#include "login.h"
#include "search.h"
#include "main_menu.h"
class NavigationService
{
private:
    QWidget* _mainFrame;
    QStack<QWidget*> _frames;
    QWidget* _current;
    QList<QWidget*> _waitDelete;
    NavigationService();
    static NavigationService _instance;
public:
    static void init(QWidget* mainFrame);
    static NavigationService& getInstance();
    void navigate(eWidget e,QObject* para=nullptr);
    void goBack();
    void goHome();
};

#endif // NAVIGATIONSERVICE_H
