#include "change_pass.h"
#include "ui_change_pass.h"
#include "navigationservice.h"
#include "../LibProLib/Manager/sessionmanager.h"
#include "../LibProLib/Manager/usermanager.h"
#include <QMessageBox>
Change_pass::Change_pass(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Change_pass)
{
    ui->setupUi(this);
}

Change_pass::~Change_pass()
{
    delete ui;
}

void Change_pass::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}

void Change_pass::on_ChangeButton_clicked()
{
    QString old=ui->OldPasswordlineEdit->text();
    QString newPass=ui->NewPasswordlineEdit->text();
    Account* current=SessionManager::Current().getAccount();
    if(old==current->getPassword())
    {
        current->setPassword(newPass);
        UserManager::Instance().ModifyAccount(*current);
        QMessageBox::information(this,"Thông báo","Đổi thành công");
        NavigationService::getInstance().goBack();
    }
    else
    {
        QMessageBox::warning(this,"Thông báo","Mật khẩu không đúng.");

    }

}
