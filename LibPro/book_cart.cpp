#include "book_cart.h"
#include "ui_book_cart.h"
#include "navigationservice.h"
#include "../LibProLib/Manager/sessionmanager.h"
#include "../LibProLib/Models/borrowedbook.h"
#include "../LibProLib/Models/book.h"
#include "../LibProLib/Manager/bookmanager.h"
#include "navigationservice.h"
#include <QStandardItemModel>
#include <QMessageBox>
Book_cart::Book_cart(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Book_cart)
{
    ui->setupUi(this);
}

Book_cart::~Book_cart()
{
    delete ui;
}

void Book_cart::showEvent(QShowEvent *event)
{
    refresh();
}

void Book_cart::on_ConfirmButton_clicked()
{
    QList<Book*> books=SessionManager::Current().getBookCart()->getAllBook();
    if(books.length()>0)
    {
         QMessageBox::StandardButton result=(QMessageBox::StandardButton)QMessageBox::question(this,"Xác nhận","Bạn có chắc chắc muôn mượn số sách trên",QMessageBox::Yes,QMessageBox::No);
         if(result==QMessageBox::No)
             return;
    }
    else
    {
        QMessageBox::information(this,"Thông báo","Bạn chưa chọn quyển sách nào.");

    }
   int uid=SessionManager::Current().getAccount()->getUserID();
    try{
       for(Book* b:books)
       {
           BorrowedBook borrowed(0,b->getBookID(),uid,QDateTime::currentDateTime().addDays(7),QDateTime::currentDateTime(),false);
           BookManager::getInstance().addBorrowedBook(borrowed);
       }
       QMessageBox::information(this,"Thông báo","Mượn sách thành công, nhớ trả sách đúng hẹn.");
       SessionManager::Current().getBookCart()->clear();
       NavigationService::getInstance().goBack();
   }
   catch(...)
   {
       QMessageBox::warning(this,"Thông báo","Đã xảy ra lỗi, vui lòng thử lại sau.");
   }
}

void Book_cart::refresh()
{
    QList<Book*> books=SessionManager::Current().getBookCart()->getAllBook();
    QStandardItemModel* model=new QStandardItemModel(books.length(),6);
    model->setHorizontalHeaderItem(0,new QStandardItem("Book Id"));
    model->setHorizontalHeaderItem(1,new QStandardItem("Title"));
    model->setHorizontalHeaderItem(2,new QStandardItem("Author"));
    model->setHorizontalHeaderItem(3,new QStandardItem("Publisher"));
    model->setHorizontalHeaderItem(4,new QStandardItem("Available"));
    model->setHorizontalHeaderItem(5,new QStandardItem("Description"));
    QStandardItem* item;

    for(int i=0;i<books.length();i++)
    {
        Book* b=books.at(i);
        item=new QStandardItem(QString::number(b->getBookID()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,0,item);


        item=new QStandardItem(b->getTitle());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,1,item);


        item=new QStandardItem(b->getAuthor());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,2,item);


        item=new QStandardItem(b->getPublisher());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,3,item);



        item=new QStandardItem(QString::number(b->getQuantity()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,4,item);

        item=new QStandardItem(b->getDecription());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);

        model->setItem(i,5,item);

    }
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(5,QHeaderView::Stretch);
}

void Book_cart::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}

void Book_cart::on_RemoveButton_clicked()
{
        QModelIndex index=ui->tableView->currentIndex();
        Book* b=SessionManager::Current().getBookCart()->getAllBook().at(index.row());
        if(QMessageBox::question(this,"Xác nhận","Bạn có chắc muốn xóa "+b->getTitle()+ " ra khỏi giỏ?",QMessageBox::Yes,QMessageBox::No)==(int)QMessageBox::Yes)
        {
            SessionManager::Current().getBookCart()->removeBook(index.row());
            refresh();
        }
}
