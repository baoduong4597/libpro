#ifndef PUNISH_H
#define PUNISH_H

#include <QWidget>

namespace Ui {
class Punish;
}

class Punish : public QWidget
{
    Q_OBJECT

public:
    explicit Punish(QWidget *parent = 0);
    ~Punish();

private:
    Ui::Punish *ui;
};

#endif // PUNISH_H
