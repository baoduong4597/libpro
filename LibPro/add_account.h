#ifndef ADD_ACCOUNT_H
#define ADD_ACCOUNT_H

#include <QWidget>
#include "../LibProLib/Manager/usermanager.h"
namespace Ui {
class Add_account;
}

class Add_account : public QWidget
{
    Q_OBJECT

public:
    explicit Add_account(QWidget *parent = 0);
    ~Add_account();

private slots:
    void on_AddButton_clicked();

    void on_BackButton_clicked();

private:
    Ui::Add_account *ui;
};

#endif // ADD_ACCOUNT_H
