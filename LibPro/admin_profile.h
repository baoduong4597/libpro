#ifndef ADMIN_PROFILE_H
#define ADMIN_PROFILE_H

#include <QWidget>

namespace Ui {
class Admin_profile;
}

class Admin_profile : public QWidget
{
    Q_OBJECT

public:
    explicit Admin_profile(QWidget *parent = 0);
    ~Admin_profile();

private slots:
    void on_AddButton_clicked();

    void on_ModifyButton_clicked();

    void on_SearchButton_clicked();

    void on_SearchUserButton_clicked();

    void on_LogoutButton_clicked();

    void on_AddUserButton_clicked();

private:
    Ui::Admin_profile *ui;
};

#endif // ADMIN_PROFILE_H
