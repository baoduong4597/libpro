#ifndef USER_PROFILE_H
#define USER_PROFILE_H

#include <QWidget>

namespace Ui {
class User_profile;
}

class User_profile : public QWidget
{
    Q_OBJECT

public:
    explicit User_profile(QWidget *parent = 0);
    ~User_profile();

private slots:
    void on_NoticeButton_clicked();

    void on_SearchButton_clicked();

    void on_BookcartButton_clicked();

    void on_ChangePasswordButton_clicked();

    void on_HistoryButton_clicked();

    void on_LogoutButton_clicked();

private:
    Ui::User_profile *ui;
};

#endif // USER_PROFILE_H
