#include "modify_book.h"
#include "ui_modify_book.h"
#include "navigationservice.h"
#include "../LibProLib/Manager/bookmanager.h"
#include <QMessageBox>
Modify_book::Modify_book(QWidget *parent,QObject* param) :
    QWidget(parent),
    ui(new Ui::Modify_book)
{
    ui->setupUi(this);
    if(param)
    {
        book=qobject_cast<Book*>(param);
        ui->TittlelineEdit->setText(book->getTitle());
        ui->AuthorlineEdit->setText(book->getAuthor());
        ui->PublisherlineEdit->setText(book->getPublisher());
        ui->QuantityBox->setValue(book->getQuantity());
        ui->DescriptionlineEdit->setText(book->getDecription());
    }
}

Modify_book::~Modify_book()
{
    delete ui;
}

void Modify_book::on_ApplyButton_clicked()
{
book->setAuthor(ui->AuthorlineEdit->text());
book->setTitle(ui->TittlelineEdit->text());
book->setDecription(ui->DescriptionlineEdit->text());
book->setQuantity(ui->QuantityBox->value());
book->setPublisher(ui->PublisherlineEdit->text());
BookManager::getInstance().modifyBook(*book);
QMessageBox::information(this,"Thông báo","Sửa thành công");
NavigationService::getInstance().goBack();
}

void Modify_book::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}
