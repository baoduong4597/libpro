#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include "../LibProLib/Manager/usermanager.h"
#include "../LibProLib/Manager/sessionmanager.h"
namespace Ui {
class Login;
}

class Login : public QWidget
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();

private slots:
    void on_LoginButton_clicked();

    void on_BackButton_clicked();


private:
    Ui::Login *ui;
};

#endif // LOGIN_H
