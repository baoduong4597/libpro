#ifndef ADD_BOOK_H
#define ADD_BOOK_H

#include <QWidget>

namespace Ui {
class Add_book;
}

class Add_book : public QWidget
{
    Q_OBJECT

public:
    explicit Add_book(QWidget *parent = 0);
    ~Add_book();

private slots:
    void on_AddButton_clicked();
    
    void on_BackButton_clicked();

private:
    Ui::Add_book *ui;
};

#endif // ADD_BOOK_H
