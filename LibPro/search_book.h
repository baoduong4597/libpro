#ifndef SEARCH_BOOK_H
#define SEARCH_BOOK_H

#include <QWidget>

namespace Ui {
class Search_book;
}

class Search_book : public QWidget
{
    Q_OBJECT

public:
    explicit Search_book(QWidget *parent = 0);
    ~Search_book();
protected:
    void showEvent(QShowEvent * event) override;
private slots:
    void on_BackButton_clicked();
    void refresh();



    void on_SearchlineEdit_returnPressed();

    void on_AddButton_clicked();

    void on_DeleteButton_clicked();

    void on_ModifyButton_clicked();

private:
    Ui::Search_book *ui;
};

#endif // SEARCH_BOOK_H
