#include "modify_account.h"
#include "ui_modify_account.h"
#include <QMessageBox>
#include "navigationservice.h"
Modify_account::Modify_account(QWidget *parent,QObject* param) :
    QWidget(parent),
    ui(new Ui::Modify_account)
{
    ui->setupUi(this);
    ui->RoleBox->addItem("Admin",QVariant(1));
    ui->RoleBox->addItem("Librarian",QVariant(2));
    ui->RoleBox->addItem("User",QVariant(3));
    acc=qobject_cast<Account*>(param);
    role=UserManager::Instance().getRoleMapByAccountId(acc->getAccountID());
    ui->UserIDlineEdit->setText(QString::number(acc->getUserID()));
    ui->UsernamelineEdit->setText(acc->getAccountName());
    ui->PasswordlineEdit->setText(acc->getPassword());
    ui->ActivecheckBox->setChecked(acc->getStatus());
    ui->RoleBox->setCurrentIndex(role->getRoleID()-1);

}

Modify_account::~Modify_account()
{
    delete ui;
}

void Modify_account::on_ApplyButton_clicked()
{
    acc->setPassword(ui->PasswordlineEdit->text());
    acc->setStatus(ui->ActivecheckBox->isChecked());
    if(role->getRoleID()!=ui->RoleBox->currentData().toInt())
    {
        role->setRoleID(ui->RoleBox->currentData().toInt());
        UserManager::Instance().ModifyRoleMap(*role);
    }
    UserManager::Instance().ModifyAccount(*acc);
    QMessageBox::information(this,"Thông báo","Sửa thành công");
    NavigationService::getInstance().goBack();
}
