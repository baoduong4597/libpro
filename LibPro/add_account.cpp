#include "add_account.h"
#include "ui_add_account.h"
#include "navigationservice.h"
#include <QMessageBox>
Add_account::Add_account(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Add_account)
{
    ui->setupUi(this);
    ui->RoleBox->addItem("User",QVariant(3));
    ui->RoleBox->addItem("Librarian",QVariant(2));
    ui->RoleBox->addItem("Admin",QVariant(1));

}

Add_account::~Add_account()
{
    delete ui;
}

void Add_account::on_AddButton_clicked()
{
    QString user=ui->UsernamelineEdit->text();
    QString pass=ui->PasswordlineEdit->text();
    int userId=ui->UserIdBox->value();
    int role=ui->RoleBox->currentData().toInt();
    Account acc(0,userId,pass,user,true);
    RoleMap roleMap(0,role);
    UserManager::Instance().AddAccount(acc,roleMap);
    if(acc.getAccountID()!=0)
    {
        QMessageBox::information(this,"Thông báo","Thêm thành công");
        NavigationService::getInstance().goBack();
    }
    else
    {
        QMessageBox::warning(this,"Thông báo","Thêm thất bại");
    }
}

void Add_account::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}
