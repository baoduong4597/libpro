#include "user_profile.h"
#include "ui_user_profile.h"
#include "navigationservice.h"
User_profile::User_profile(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::User_profile)
{
    ui->setupUi(this);
}

User_profile::~User_profile()
{
    delete ui;
}

void User_profile::on_NoticeButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WNotice);
}

void User_profile::on_SearchButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WSearch_book);

}

void User_profile::on_BookcartButton_clicked()
{
NavigationService::getInstance().navigate(eWidget::WBook_cart);
}

void User_profile::on_ChangePasswordButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WChange_pass);
}

void User_profile::on_HistoryButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WHistory);
}

void User_profile::on_LogoutButton_clicked()
{
    SessionManager::Current().logout();
    NavigationService::getInstance().goHome();
}
