#-------------------------------------------------
#
# Project created by QtCreator 2017-06-20T15:59:48
#
#-------------------------------------------------

QT       -= gui

TARGET = LibProLib
TEMPLATE = lib
CONFIG += staticlib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Common/filehelper.cpp \
    Common/filehelper.cpp \
    Models/account.cpp \
    Models/book.cpp \
    Models/bookcart.cpp \
    Models/bookcartdetail.cpp \
    Models/borrowedbook.cpp \
    Models/message.cpp \
    Models/role.cpp \
    Models/rolemap.cpp \
    Models/user.cpp \
    Manager/usermanager.cpp \
    Manager/sessionmanager.cpp \
    Models/bookdetail.cpp \
    Manager/bookmanager.cpp \
    Manager/messagemanager.cpp

HEADERS += \
    Common/filehelper.h \
    Common/filehelper.h \
    Models/account.h \
    Models/book.h \
    Models/bookcart.h \
    Models/bookcartdetail.h \
    Models/borrowedbook.h \
    Models/message.h \
    Models/role.h \
    Models/rolemap.h \
    Models/user.h \
    Manager/usermanager.h \
    Manager/sessionmanager.h \
    Models/session.h \
    Models/bookdetail.h \
    Manager/bookmanager.h \
    Manager/messagemanager.h \
    Models/config.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
