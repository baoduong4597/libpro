#include "filehelper.h"
#include <QTextStream>
FileHelper::FileHelper()
{

}

QStringList FileHelper::ReadAllLine(QString filePath)
{
    QFile file(filePath);
    QStringList result;

    if(file.open(QFile::ReadOnly))
    {
       while(!file.atEnd())
       {
            QString line(file.readLine());
            line.remove('\n');
            line.remove('\r');
            if(!line.isNull() && !line.isEmpty())
            {
                result.append(line);
            }
       }
    }
    return result;
}

bool FileHelper::SaveAllLine(QString filePath, QStringList lines)
{
QFile file(filePath);
if(file.open(QFile::WriteOnly))
{
   QTextStream st(&file);
   st.setCodec("UTF-8");
   for(QString line : lines)
   {
        st << line << "\n";
   }
   return true;
}
return false;
}
