#include "messagemanager.h"
#include "../Models/config.h"
#include "../Common/filehelper.h"

MessageManager MessageManager::_instance=MessageManager();
void MessageManager::loadMessage(QString file)
{
    QStringList lines=FileHelper::ReadAllLine(file);
    for(QString line: lines)
    {
        try
        {
            Message* u=new Message(line);
            _message.append(u);
        }
        catch(...)
        {
            continue;
        }
    }
}

void MessageManager::saveMessage(QString file)
{
    QStringList data;
    for(Message* b:_message)
    {
        data.append(b->toString());
    }
    FileHelper::SaveAllLine(file,data);
}

MessageManager::MessageManager()
{
    QString folder=DataFolder;
    _messageFile =folder+"Messages.txt";
    loadMessage(_messageFile);
}

MessageManager::~MessageManager()
{
saveMessage(_messageFile);
}

MessageManager &MessageManager::getInstance()
{
    return _instance;
}

QList<Message *> MessageManager::getMessageByUser(int userid)
{
    QList<Message*> result;
    for(Message* m:_message)
    {
        if(m->getReceived()==userid)
        {
            result.append(m);
        }
    }
    return result;
}

Message *MessageManager::getMessageById(int id)
{
    for(Message* m:_message)
    {
        if(m->getMessId()==id)
            return m;
    }
    return nullptr;
}

void MessageManager::addMessage(Message &m)
{
    int lastId=_message.last()->getMessId();
    lastId++;
    m.setMessId(lastId);
    _message.append(new Message(m.toString()));
}

void MessageManager::removeMessage(int messId)
{
    Message* msg=getMessageById(messId);
    if(msg)
        _message.removeOne(msg);
}
