#ifndef SESSIONMANAGER_H
#define SESSIONMANAGER_H
#include "../Models/bookcart.h"
#include "../Models/account.h"
#include "../Models/rolemap.h"

class SessionManager
{
private:
    Account* _account;
    RoleMap* _role;
    BookCart* _bookCart;
    static SessionManager _session;
    SessionManager(SessionManager const&);
    void operator=(SessionManager const &);
public:
    SessionManager();
    static SessionManager& Current();
    void setAccount(Account* acc);
    void setRoleMap(RoleMap* role);
    void logout();
    Account* getAccount();
    RoleMap* getRoleMap();
    BookCart *getBookCart();
    ~SessionManager();
};

#endif // SESSIONMANAGER_H
