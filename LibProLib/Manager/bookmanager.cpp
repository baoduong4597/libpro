#include "bookmanager.h"
#include "../Common/filehelper.h"
#include "../Models/config.h"
BookManager BookManager::_instance=BookManager();
void BookManager::loadBooks(QString file)
{
    QStringList lines=FileHelper::ReadAllLine(file);
    for(QString line: lines)
    {
        try
        {
            Book* u=new Book(line);
            _books.append(u);
        }
        catch(...)
        {
            continue;
        }
    }
}

void BookManager::loadBorrowedBooks(QString file)
{
    QStringList lines=FileHelper::ReadAllLine(file);
    for(QString line: lines)
    {
        try
        {
            BorrowedBook* u=new BorrowedBook(line);
            _borrowedBooks.append(u);
        }
        catch(...)
        {
            continue;
        }
    }
}



bool BookManager::saveBooks(QString file)
{

    QStringList data;
    for(Book* b:_books)
    {
        data.append(b->toString());
    }
    return FileHelper::SaveAllLine(file,data);
}


bool BookManager::saveBorrowedBook(QString file)
{
    QStringList data;
    for(BorrowedBook* b:_borrowedBooks)
    {
        data.append(b->toString());
    }
    return FileHelper::SaveAllLine(file,data);
}



BookManager::BookManager()
{

    _bookFile=DataFolder+"Book.txt";
    _borrowedBookFile=DataFolder+"BorrowedBook.txt";
    loadBooks(_bookFile);
    loadBorrowedBooks(_borrowedBookFile);
}

BookManager::~BookManager()
{
    saveBooks(_bookFile);
    saveBorrowedBook(_borrowedBookFile);
    for(Book* b:_books)
        delete b;
    for(BorrowedBook* b:_borrowedBooks)
        delete b;
    _books.clear();
    _borrowedBooks.clear();
}

QList<Book *> BookManager::Books()
{
    return _books;
}

Book *BookManager::getBookById(int bookId)
{
    for(Book* b:_books)
    {
        if(b->getBookID()==bookId)
            return b;
    }
    return nullptr;
}

QList<BorrowedBook *> BookManager::getBorrowedBooksByBookId(int bookId)
{
    QList<BorrowedBook *> result;
    for(BorrowedBook* b:_borrowedBooks)
    {
        if(b->getBookID()==bookId)
            result.append(b);
    }
    return result;
}

BorrowedBook *BookManager::getBorrowedBookById(int borrowedId)
{
    for(BorrowedBook* b:_borrowedBooks)
    {
        if(b->getBorrowedID()==borrowedId)
            return b;
    }
    return nullptr;
}

QList<BorrowedBook *> BookManager::getAllBorrowedBook(int userId)
{
    QList<BorrowedBook *> result;
    for(BorrowedBook* b:_borrowedBooks)
    {
        if(b->getUserID()==userId)
            result.append(b);
    }
    return result;
}

QList<BorrowedBook *> BookManager::getAllBorrowedBookByTitle(QString title)
{
QList<BorrowedBook*> result;
for(BorrowedBook* b:_borrowedBooks)
{
    Book* book=getBookById(b->getBookID());
    if(book && book->getTitle().contains(title,Qt::CaseInsensitive))
    {
        result.append(b);
    }
}
return result;
}

QList<Book *> BookManager::getBooksByTitle(QString name)
{
    QList<Book*> result;
    for(Book* book:_books)
    {
        if(book->getTitle().contains(name,Qt::CaseInsensitive))
        {
            result.append(book);
        }
    }
    return result;
}



void BookManager::addBook(Book &book)
{
    int lastid=0;
    if(_books.length()>0)
    {
        lastid=_books.last()->getBookID();
    }
    lastid++;
    book.setBookID(lastid);
    Book* b=new Book(book.toString());
    _books.append(b);
}

void BookManager::modifyBook(Book &book)
{
    Book* b=getBookById(book.getBookID());
    b->setAuthor(book.getAuthor());
    b->setBorrowedCount(book.getBorrowedCount());
    b->setDecription(book.getDecription());
    b->setPublisher(book.getPublisher());
    b->setQuantity(book.getQuantity());
    b->setTitle(b->getTitle());
}

void BookManager::removeBook(int bookId)
{
    Book* b=getBookById(bookId);
    _books.removeOne(b);
    QList<BorrowedBook*> list=getBorrowedBooksByBookId(bookId);
    for(BorrowedBook* b:list)
    {
        _borrowedBooks.removeOne(b);
        delete b;
    }
}

void BookManager::addBorrowedBook(BorrowedBook &book)
{
    int lastid=0;
    if(_borrowedBooks.length()>0)
    {
       lastid =_borrowedBooks.last()->getBorrowedID();
    }
    lastid++;
    book.setBorrowedID(lastid);
    Book* b=getBookById(book.getBookID());
    b->setBorrowedCount(b->getBorrowedCount()+1);
    _borrowedBooks.append(new BorrowedBook(book.toString()));
}

void BookManager::modifyBorrowedBook(BorrowedBook &book)
{
    BorrowedBook* b=getBorrowedBookById(book.getBorrowedID());
}

void BookManager::removeBorrowedBook(int id)
{
        BorrowedBook* b=getBorrowedBookById(id);
        if(b)
            _borrowedBooks.removeOne(b);
}

BookManager &BookManager::getInstance()
{
    return _instance;
}

QList<BorrowedBook *> BookManager::getExpiredBooks()
{
    QList<BorrowedBook* > result ;

    for(BorrowedBook* b:_borrowedBooks)
    {
        if(b->getExpiryDate()< QDateTime::currentDateTime() && b->getStatus()==false)
        {
            result.append(b);;
        }
    }
    return result ;
}
