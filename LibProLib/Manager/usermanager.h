#ifndef USERMANAGER_H
#define USERMANAGER_H
#include "../Models/user.h"
#include "../Models/account.h"
#include "../Models/role.h"
#include "../Models/rolemap.h"
#include "../Common/filehelper.h"
#include <QList>
class UserManager
{
private:
    QList<User*> _users;
    QList<Account*> _accounts;
    QList<Role*> _roles;
    QList<RoleMap*> _roleMaps;
    QString _userfile;
    QString _accountFile;
    QString _roleFile;
    QString _roleMapFile;
    void loadUsers(QString file);
    void loadAccounts(QString file);
    void loadRoles(QString file);
    void loadRoleMaps(QString file);
    bool saveUsers(QString file);
    bool saveAccounts(QString file);
    bool saveRole(QString file);
    bool saveRoleMap(QString file);
    static UserManager _instance;
    bool _isUserDirty;
    bool _isAccountDirty;
    bool _isRoleDirty;
    bool _isRoleMapDirty;
    UserManager(UserManager const&);
    void operator=(UserManager const &);
public:
    QList<User*>& Users();
    QList<Account*> & Accounts();
    QList<Role*>& Roles();
    QList<RoleMap*>& RoleMaps();
    bool SaveToFile();
    static UserManager& Instance();
    UserManager();
    QList<User*> getAllUserByName(QString name);
    QList<Account*> getAllAccountByAccName(QString name);
    Account* getAccountByName(QString accountName);
    Account* getAccountByAccountId(int id);
    RoleMap *getRoleMapByAccountId(int accId);
    User* getUserByID(int userid);

    //Login result:
    //0: susscess
    //1:Sai username
    //2: Sai pass
    //3: Disable
    int login(QString accountName,QString pass);
    QList<Account *> getAccountsByUser(int uid);
    bool AddUser(User& u);
    bool ModifyUser(User& u);
    bool Remove(int userID);
    bool AddAccount(Account& acc, RoleMap& roleMap);
    bool ModifyAccount(Account& acc);
    bool ModifyRoleMap(RoleMap& roleMap);
    bool RemoveAccount(int accId);

    Role* getRoleById(int roleId);
    ~UserManager();
};



#endif // USERMANAGER_H
