#include "sessionmanager.h"
SessionManager SessionManager::_session=SessionManager();
SessionManager::SessionManager()
{
_bookCart=new BookCart();
}

SessionManager &SessionManager::Current()
{
   return _session;
}


SessionManager::~SessionManager()
{

}

void SessionManager::setAccount(Account *acc)
{
    _account=acc;
}

void SessionManager::setRoleMap(RoleMap *role)
{
    _role=role;
}

void SessionManager::logout()
{
    _account=nullptr;
    _role=nullptr;
    _bookCart->clear();
}

Account* SessionManager::getAccount()
{
    return _account;
}

RoleMap* SessionManager::getRoleMap()
{
    return _role;
}

BookCart *SessionManager::getBookCart()
{
    return _bookCart;
}

