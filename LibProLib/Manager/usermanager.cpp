#include "usermanager.h"
#include "../Models/config.h"
UserManager UserManager::_instance=UserManager();
void UserManager::loadUsers(QString file)
{

    QStringList lines=FileHelper::ReadAllLine(file);
    for(QString line: lines)
    {
        try
        {
            User* u=new User(line);
            _users.append(u);
        }
        catch(...)
        {
            continue;
        }
    }
}

void UserManager::loadAccounts(QString file)
{
    QStringList lines=FileHelper::ReadAllLine(file);
    for(QString line: lines)
    {
        try
        {
            Account* a=new Account(line);
            _accounts.append(a);
        }
        catch(...)
        {
            continue;
        }
    }
}

void UserManager::loadRoles(QString file)
{
    _roles.append(new Role(1,"Admin"));
    _roles.append(new Role(2,"Librarian"));
    _roles.append(new Role(3,"User"));
    _isRoleDirty=true;
}

void UserManager::loadRoleMaps(QString file)
{
    QStringList lines=FileHelper::ReadAllLine(file);
    for(QString line: lines)
    {
        try
        {
            RoleMap* m=new RoleMap(line);
            _roleMaps.append(m);
        }
        catch(...)
        {
            continue;
        }
    }
}

bool UserManager::saveUsers(QString file)
{
    QStringList userData;
    for(User* u:_users)
    {
        userData.append(u->toString());
    }
    return FileHelper::SaveAllLine(file,userData);
}

bool UserManager::saveAccounts(QString file)
{
    QStringList accountData;
    for(Account* acc:_accounts)
    {
        accountData.append(acc->toString());
    }
    return FileHelper::SaveAllLine(file,accountData);

}

bool UserManager::saveRole(QString file)
{
    QStringList roleData;
    for(Role* r:_roles)
    {
        roleData.append(r->toString());
    }
    return FileHelper::SaveAllLine(file,roleData);
}

bool UserManager::saveRoleMap(QString file)
{
    QStringList roleMapData;
    for(RoleMap* r:_roleMaps)
    {
        roleMapData.append(r->toString());
    }
    return FileHelper::SaveAllLine(file,roleMapData);
}

UserManager::UserManager()
{
    _userfile=DataFolder+"users.txt";
    _accountFile=DataFolder+"accounts.txt";
    _roleFile=DataFolder+"roles.txt";
    _roleMapFile=DataFolder+"rolemaps.txt";
    loadUsers(_userfile);
    loadAccounts(_accountFile);
    loadRoles(_roleFile);
    loadRoleMaps(_roleMapFile);
    _isUserDirty=false;
    _isAccountDirty=false;
    _isRoleDirty=false;
    _isRoleMapDirty=false;
}

QList<User *> UserManager::getAllUserByName(QString name)
{
    QList<User *> result;
    for(User * u:_users)
    {
        if(u->getFullName().contains(name,Qt::CaseInsensitive))
            result.append(u);
    }
    return result;
}

QList<Account *> UserManager::getAllAccountByAccName(QString name)
{
    QList<Account *> result;
    for(Account * u:_accounts)
    {
        if(u->getAccountName().contains(name,Qt::CaseInsensitive))
            result.append(u);
    }
    return result;
}

QList<User*>& UserManager::Users()
{
    return _users;
}

QList<Account*>& UserManager::Accounts()
{
    return _accounts;

}

QList<Role*>& UserManager::Roles()
{
    return _roles;
}

QList<RoleMap*>& UserManager::RoleMaps()
{
    return _roleMaps;
}



bool UserManager::SaveToFile()
{
    bool ret=true;
    if(_isUserDirty)
        ret&=saveUsers(_userfile);

    if(_isAccountDirty)
        ret&=saveAccounts(_accountFile);

    if(_isRoleDirty)
        ret&=saveRole(_roleFile);

    if(_isRoleMapDirty)
        ret&=saveRoleMap(_roleMapFile);

    return ret;
}

UserManager& UserManager::Instance()
{
    return _instance;
}

Account* UserManager::getAccountByName(QString accountName)
{
    for(Account* acc:_accounts)
    {
        if(acc->getAccountName()==accountName)
        {
            return acc;
        }
    }
    return nullptr;
}

Account *UserManager::getAccountByAccountId(int id)
{
    for(Account* acc:_accounts)
    {
        if(acc->getAccountID()==id)
            return acc;
    }
    return nullptr;
}

RoleMap *UserManager::getRoleMapByAccountId(int accId)
{
    for(RoleMap* m:_roleMaps)
    {
        if(m->getAccountID()==accId)
        {
            return m;
        }
    }
    return nullptr;
}

User *UserManager::getUserByID(int userid)
{
    for(User* u:_users)
    {
        if(u->getUserId()==userid)
            return u;
    }
    return nullptr;
}

int UserManager::login(QString username, QString pass)
{
    Account* acc=getAccountByName(username);
    if(acc==nullptr)
        return 1;
    if(acc->getPassword()!=pass)
        return 2;
    if(!acc->getStatus())
        return 3;
    return 0;
}

QList<Account *> UserManager::getAccountsByUser(int uid)
{
    QList<Account*> result;
    for(Account* acc:_accounts)
    {
        if(acc->getUserID()==uid)
            result.append(acc);
    }
    return result;
}

bool UserManager::AddUser(User &u)
{
    try
    {
        int lastId= _users.last()->getUserId();
        lastId++;
        User* user=new User(u.toString());
        user->setUserId(lastId);
        _users.append(user);
        _isUserDirty=true;
        return true;
    }
    catch(...)
    {
        return false;
    }
}

bool UserManager::ModifyUser(User &u)
{
    User* current=getUserByID(u.getUserId());
    if(!current)
        return false;
    current->setBirthday(u.getBirthday());
    current->setEmail(u.getEmail());
    current->setFullName(u.getFullName());
    current->setIdentifyNumber(u.getIdentifyNumber());
    current->setJob(u.getJob());
    return true;
    _isUserDirty=true;
}

bool UserManager::Remove(int userID)
{
    User* current=getUserByID(userID);
    if(!current) return false;
    try
    {
        bool ret=_users.removeOne(current);
        QList<Account*> accs=getAccountsByUser(userID);
        for(Account* acc:accs)
        {
            _accounts.removeOne(acc);
            delete acc;
        }

        _isAccountDirty=true;
        _isUserDirty=true;
        return ret;
    }
    catch(...)
    {
        return false;
    }
}

bool UserManager::AddAccount(Account &acc, RoleMap &roleMap)
{
    try
    {
        Account* exist=getAccountByName(acc.getAccountName());
        if(exist)
            return false;
        int lastAccId=0;
        if(_accounts.length()>0)
        {
            lastAccId=_accounts.last()->getAccountID();
        }
        lastAccId++;
        acc.setAccountID(lastAccId);
        roleMap.setAccountID(lastAccId);
        Account* newAcc=new Account(acc.toString());
        RoleMap* newRoleMap =new RoleMap(roleMap.toString());
        _accounts.append(newAcc);
        _roleMaps.append(newRoleMap);
        _isAccountDirty=true;
        _isRoleMapDirty=true;
        return true;
    }
    catch(...)
    {
        return false;
    }
}

bool UserManager::ModifyAccount(Account &acc)
{
    try
    {
        Account* old=getAccountByAccountId(acc.getAccountID());
        old->setPassword(acc.getPassword());
        old->setStatus(acc.getStatus());
        _isAccountDirty=true;
        return true;
    }
    catch(...)
    {
        return false;
    }
}

bool UserManager::ModifyRoleMap(RoleMap &roleMap)
{
    RoleMap* current=getRoleMapByAccountId(roleMap.getAccountID());
    current->setRoleID(roleMap.getRoleID());
    _isRoleMapDirty=true;
    return true;
}

bool UserManager::RemoveAccount(int accId)
{
    Account* acc=getAccountByAccountId(accId);
    if(!acc)
        return false;

    try
    {
        RoleMap* role=getRoleMapByAccountId(accId);
        bool ret= _accounts.removeOne(acc);
        ret&=_roleMaps.removeOne(role);
        _isRoleMapDirty=true;
        _isAccountDirty=true;
        return ret;
    }
    catch(...)
    {
        return false;
    }
}

Role *UserManager::getRoleById(int roleId)
{
    for(Role* r:_roles)
    {
        if(r->getRoleID()==roleId)
            return r;
    }
    return nullptr;
}



UserManager::~UserManager()
{
    SaveToFile();
    for(User* u:_users){
        delete u;
    }
    for(Account* a:_accounts)
    {
        delete a;
    }
    for(Role* r:_roles)
    {
        delete r;
    }
    for (RoleMap* m:_roleMaps){
        delete m;
    }
    _users.clear();
    _accounts.clear();
    _roles.clear();
    _roleMaps.clear();
}
