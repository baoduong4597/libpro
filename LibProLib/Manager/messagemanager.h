#ifndef MESSAGEMANAGER_H
#define MESSAGEMANAGER_H

#include "../Models/message.h"
#include "../Models/user.h"
#include <QList>
class MessageManager
{
private:
    QList<Message*> _message;
    QString _messageFile;
    void loadMessage(QString file);
    bool _isDirty;
    void saveMessage(QString file);
    static MessageManager _instance;
    MessageManager(MessageManager const&);
    MessageManager();
    void operator =(MessageManager const&);
public:
    ~MessageManager();
    static MessageManager& getInstance();
    QList<Message*> getMessageByUser(int userid);
    Message* getMessageById(int id);
    void addMessage(Message& m);
    void removeMessage(int messId);

};

#endif // MESSAGEMANAGER_H
