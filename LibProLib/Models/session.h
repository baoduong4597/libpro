#ifndef SESSION_H
#define SESSION_H
#include "../Models/account.h"
#include "../Models/rolemap.h"
#include "../Models/bookcart.h"
class Session
{
private:

    Session(Session const &);
    void operator=(Session const &);
public:
    Session();
    Session(Account* acc,RoleMap* role);

    ~Session();
};

#endif // SESSION_H
