#ifndef BOOK_H
#define BOOK_H
#include <QString>
#include <QTextStream>
#include <QObject>

class Book:public QObject
{
    Q_OBJECT
private:
    int _BookID;
    QString _Title;
    QString _Decription;
    QString _Author;
    QString _Publisher;
    int _Quantity;
    int _BorrowedCount;
public:
    Book();
    Book(int id, QString tit, QString dec, QString author, QString pub,int quan, int bocount);
    Book(QString data);
    int getBookID();
    void setBookID(int val);
    QString getTitle();
    void setTitle(QString val);
    QString getDecription();
    void setDecription(QString val);
    QString getAuthor();
    void setAuthor(QString val);
    QString getPublisher();
    void setPublisher(QString val);
    int getQuantity();
    void setQuantity(int val);
    int getAvailable();
    int getBorrowedCount();
    void setBorrowedCount(int val);
    QString toString();

};

#endif // BOOK_H
