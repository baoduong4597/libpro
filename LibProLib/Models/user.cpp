#include "user.h"

User::User()
{

}

User::User(int id, QString IdNu, QString fname, QDateTime dati, QString job, QString email)
{
    _userID=id;
    _IdentifyNumber=IdNu;
    _FullName=fname;
    _Birthday=dati;
    _Job=job;
    _Email=email;
}

User::User(QString data)
{
    QStringList values=data.split('|');
    _userID=((QString)values.at(0)).toInt();
    _IdentifyNumber=QString(values.at(1));
    _FullName=QString(values.at(2));
    _Birthday=QDateTime::fromString((QString)values.at(3),Qt::ISODate);
    _Job=QString(values.at(4));
    _Email=QString(values.at(5));
}

int User::getUserId()
{
    return _userID;
}

void User::setUserId(int val)
{
    _userID=val;
}

QString User::getIdentifyNumber()
{
    return _IdentifyNumber;
}

void User::setIdentifyNumber(QString val)
{
    _IdentifyNumber=val;
}

QString User::getFullName()
{
    return _FullName;
}

void User::setFullName(QString val)
{
    _FullName=val;
}

QDateTime User::getBirthday()
{
    return _Birthday;
}

void User::setBirthday(QDateTime val)
{
    _Birthday=val;
}

QString User::getJob()
{
    return _Job;
}

void User::setJob(QString val)
{
    _Job=val;
}

QString User::getEmail()
{
    return _Email;
}

void User::setEmail(QString val)
{
    _Email=val;
}

QString User::toString()
{
    QString result;
    QTextStream ts(&result);
    ts.setCodec("UTF-8");

    ts << getUserId() <<'|' <<getIdentifyNumber() << '|' << getFullName() <<'|'<<getBirthday().toString(Qt::ISODate) << '|' << getJob() << '|'<<getEmail();
    return result;
}
