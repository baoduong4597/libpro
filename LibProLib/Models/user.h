#ifndef USER_H
#define USER_H
#include <QString>
#include <QDateTime>
#include <QTextStream>
class User
{
private:
   int _userID;
   QString _IdentifyNumber;
   QString _FullName;
   QDateTime _Birthday;
   QString _Job;
   QString _Email;
public:
    User();
    User(int id, QString IdNu, QString fname, QDateTime dati, QString job, QString email);
    User(QString data);
    int getUserId();
    void setUserId(int val);
    QString getIdentifyNumber();
    void setIdentifyNumber(QString val);
    QString getFullName();
    void setFullName(QString val);
    QDateTime getBirthday();
    void setBirthday(QDateTime val);
    QString getJob();
    void setJob(QString val);\
    QString getEmail();
    void setEmail(QString val);
    QString toString();
};

#endif // USER_H
