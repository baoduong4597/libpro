#include "rolemap.h"

RoleMap::RoleMap()
{

}

RoleMap::RoleMap(int AcID, int RoID)
{
    _AccountID=AcID;
    _RoleID=RoID;
}

RoleMap::RoleMap(QString data)
{
    QStringList values=data.split('|');
    _AccountID=((QString)values.at(0)).toInt();
    _RoleID=((QString)values.at(1)).toInt();
}

int RoleMap::getAccountID()
{
    return _AccountID;
}

void RoleMap::setAccountID(int val)
{
    _AccountID=val;
}

int RoleMap::getRoleID()
{
    return _RoleID;
}

void RoleMap::setRoleID(int val)
{
    _RoleID=val;
}

QString RoleMap::toString()
{
    QString result;
    QTextStream ts(&result);
    ts.setCodec("UTF-8");

    ts << _AccountID<< "|" << _RoleID;
    return result;
}


