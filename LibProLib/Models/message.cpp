#include "message.h"

Message::Message()
{

}

Message::Message(int messId,int id, int rec, QString cont)
{
    _MessId=messId;
    _SendID=id;
    _Received=rec;
    _Content=cont;
}

Message::Message(QString data)
{
    QStringList values=data.split('|');
    _MessId=((QString)values.at(0)).toInt();
    _SendID=((QString)values.at(1)).toInt();
    _Received=((QString)values.at(2)).toInt();
    _sentDate=QDateTime::fromString((QString)values.at(3),Qt::ISODate);
    _Content=(QString)values.at(4);
}

int Message::getSendID()
{
    return _SendID;
}

void Message::setSendID(int val)
{
    _SendID=val;
}

int Message::getReceived()
{
    return _Received;
}

int Message::getMessId()
{
    return _MessId;
}

void Message::setMessId(int id)
{
    _MessId=id;
}

void Message::setReceived(int val)
{
    _Received=val;
}

QString Message::getContent()
{
    return _Content;
}

void Message::setContent(QString val)
{
    _Content=val;
}

QDateTime Message::getSentDate()
{
    return _sentDate;
}

void Message::setSentDate(QDateTime val)
{
    _sentDate=val;
}

QString Message::toString()
{
    QString result;
    QTextStream ts(&result);
    ts.setCodec("UTF-8");
    ts << _MessId<< '|' << _SendID << "|" << _Received << '|'<< _sentDate.toString(Qt::ISODate) << '|' << _Content;
    return result;

}
