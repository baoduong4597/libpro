#ifndef ROLE_H
#define ROLE_H
#include <QString>
#include <QTextStream>
class Role
{
private:
    int _RoleID;
    QString _RoleDescription;
public:
    Role();
    Role(int id,QString desc);
    Role(QString data);
    int getRoleID();
    void setRoleID(int val);
    QString getRoleDecription();
    void setRoleDecription(QString val);
    QString toString();
};

#endif // ROLE_H
