#include "bookcartdetail.h"

BookCartDetail::BookCartDetail()
{

}

BookCartDetail::BookCartDetail(int CaDe, int Cart, int book)
{
    _CartDetailID=CaDe;
    _CartID=Cart;
    _BookID=book;
}

BookCartDetail::BookCartDetail(QString data)
{
    QStringList values=data.split('|');
    _CartDetailID=((QString)values.at(0)).toInt();
    _CartID=((QString)values.at(1)).toInt();
    _BookID=((QString)values.at(2)).toInt();
}

int BookCartDetail::getCartDetailld()
{
    return _CartDetailID;
}

void BookCartDetail::setCartDetailld(int val)
{
    _CartDetailID=val;
}

int BookCartDetail::getCartld()
{
      return _CartID;
}

void BookCartDetail::setCartld(int val)
{
    _CartID=val;
}

int BookCartDetail::getBookID()
{
    return _BookID;
}

void BookCartDetail::setBookld(int val)
{
    _BookID=val;
}

QString BookCartDetail::toString()
{
    QString result;
    QTextStream ts(&result);
    ts.setCodec("UTF-8");

    ts << _CartDetailID << "|" << _CartID << '|'<< _BookID;
    return result;
}
