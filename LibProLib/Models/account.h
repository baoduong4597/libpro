#ifndef ACCOUNT_H
#define ACCOUNT_H
#include <QString>
#include <QTextStream>


class Account:public QObject
{
    Q_OBJECT
private:
    int _AccountID;
    int _UserID;
    QString _Password;
    QString _AccountName;
    bool _Status;
public:
    Account();
    Account(int id, int uid, QString pass, QString AccName,bool stt);
    Account(QString data);
    int getAccountID();
    void setAccountID(int val);
    int getUserID();
    void setUserID(int val);
    QString getPassword();
    void setPassword(QString val);
    QString getAccountName();
    void setAccountName(QString val);
    bool getStatus();
    void setStatus(bool val);
    QString toString();

};

#endif // ACCOUNT_H
