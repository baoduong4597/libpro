#include "account.h"

Account::Account()
{

}

Account::Account(int id, int uid, QString pass, QString AccName, bool stt)
{
    _AccountID=id;
    _UserID=uid;
    _Password=pass;
    _AccountName=AccName;
    _Status=stt;
}

Account::Account(QString data)
{
    QStringList values=data.split('|');
    _AccountID=((QString)values.at(0)).toInt();
    _UserID=((QString)values.at(1)).toInt();
    _Password=(QString)values.at(2);
    _AccountName=(QString)values.at(3);
    _Status=((QString)values.at(4)).toInt()!=0;
}

int Account::getAccountID()
{
    return _AccountID;
}

void Account::setAccountID(int val)
{
    _AccountID=val;
}

int Account::getUserID()
{
    return _UserID;
}

void Account::setUserID(int val)
{
    _UserID=val;
}

QString Account::getPassword()
{
    return _Password;
}

void Account::setPassword(QString val)
{
    _Password=val;
}

QString Account::getAccountName()
{
    return _AccountName;
}

void Account::setAccountName(QString val)
{
    _AccountName=val;
}

bool Account::getStatus()
{
    return _Status;
}

void Account::setStatus(bool val)
{
    _Status=val;
}

QString Account::toString()
{
    QString result;
    QTextStream ts(&result);
    ts.setCodec("UTF-8");

    ts << _AccountID << '|'<<_UserID << '|'<<_Password << '|' << _AccountName <<'|'<<_Status;
    return result;

}
