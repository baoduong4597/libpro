#include "bookcart.h"

BookCart::BookCart()
{

}

void BookCart::addBook(Book *b)
{
    _books.append(b);
}

QList<Book *> BookCart::getAllBook()
{
    return _books;
}

void BookCart::removeBook(Book *b)
{
    _books.removeOne(b);
}

void BookCart::removeBook(int index)
{
    _books.removeAt(index);
}

void BookCart::clear()
{
    _books.clear();
}

