#include "book.h"

Book::Book()
{

}

Book::Book(int id, QString tit, QString dec, QString author, QString pub, int quan, int bocount)
{
    _BookID=id;
    _Title = tit;
    _Decription=dec;
    _Author=author;
    _Publisher=pub;
    _Quantity=quan;
    _BorrowedCount=bocount;
}

Book::Book(QString data)
{
    QStringList values=data.split('|');
    _BookID=((QString)values.at(0)).toInt();
    _Title=(QString)values.at(1);
    _Decription=(QString)values.at(2);
    _Author=(QString)values.at(3);
    _Publisher=(QString)values.at(4);
    _Quantity=((QString)values.at(5)).toInt();
    _BorrowedCount=((QString)values.at(6)).toInt();
}

int Book::getBookID()
{
    return _BookID;
}

void Book::setBookID(int val)
{
    _BookID=val;
}

QString Book::getTitle()
{
    return _Title;
}

void Book::setTitle(QString val)
{
    _Title=val;
}

QString Book::getDecription()
{
    return _Decription;
}

void Book::setDecription(QString val)
{
    _Decription=val;
}

QString Book::getAuthor()
{
    return _Author;
}

void Book::setAuthor(QString val)
{
    _Author=val;
}

QString Book::getPublisher()
{
    return _Publisher;
}

void Book::setPublisher(QString val)
{
    _Publisher=val;
}

int Book::getQuantity()
{
    return _Quantity;
}


void Book::setQuantity(int val)
{
    _Quantity=val;
}
int Book::getBorrowedCount()
{
    return _BorrowedCount;
}

int Book::getAvailable()
{
    return _Quantity-_BorrowedCount;
}

void Book::setBorrowedCount(int val)
{
    _BorrowedCount=val;
}
QString Book::toString()
{
    QString result;
    QTextStream ts(&result);
    ts.setCodec("UTF-8");
    ts << _BookID << "|" << _Title << '|'<< _Decription << '|'<< _Author << '|'<<_Publisher << '|'<<_Quantity << '|'<<_BorrowedCount;
    return result;
}
