#ifndef MESSAGE_H
#define MESSAGE_H
#include <QString>
#include <QTextStream>
#include <QDateTime>
class Message
{
private:
    int _MessId;
    int _SendID;
    int _Received;
    QString _Content;
    QDateTime _sentDate;
public:
    Message();
    Message(int messId,int id, int rec, QString cont);
    Message(QString data);
    int getSendID();
    void setSendID(int val);
    int getReceived();
    int getMessId();
    void setMessId(int id);
    void setReceived(int val);
    QString getContent();
    void setContent(QString val);
    QDateTime getSentDate();
    void setSentDate(QDateTime val);
    QString toString();
};

#endif // MESSAGE_H
