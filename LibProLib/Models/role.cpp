#include "role.h"

Role::Role()
{

}

Role::Role(int id, QString desc)
{
    _RoleID=id;
    _RoleDescription=desc;
}

Role::Role(QString data)
{
    QStringList values=data.split('|');
    _RoleID=((QString)values.at(0)).toInt();
    _RoleDescription=(QString)values.at(1);
}

int Role::getRoleID()
{
    return _RoleID;
}

void Role::setRoleID(int val)
{
    _RoleID=val;
}

QString Role::getRoleDecription()
{
    return _RoleDescription;
}

void Role::setRoleDecription(QString val)
{
    _RoleDescription=val;
}

QString Role::toString()
{
    QString result;
    QTextStream ts(&result);
    ts.setCodec("UTF-8");

    ts << _RoleID << "|" << _RoleDescription;
    return result;

}
