#ifndef BORROWEDBOOK_H
#define BORROWEDBOOK_H
#include <QString>
#include <QTextStream>
#include <QDatetime>


class BorrowedBook
{
private:
    int _BorrowedID;
    int _BookID;
    int _UserID;
    QDateTime _ExpiryDate;
    QDateTime _BorrowedDate;
    bool _status;

public:
    BorrowedBook();
    BorrowedBook(int BorID,int BoID,int UsID,QDateTime ExDa,QDateTime borrowedDate,bool status);
    BorrowedBook(QString data);
    int getBorrowedID();
    void setBorrowedID(int val);
    int getBookID();
    void setBookID(int val);
    int getUserID();
    void setUserID(int val);
    QDateTime getBorrowedDate();
    QDateTime getExpiryDate();
    void setExpiryDate(QDateTime val);
    bool getStatus();
    void setStatus(bool val);
    QString toString();


};

#endif // BORROWEDBOOK_H
