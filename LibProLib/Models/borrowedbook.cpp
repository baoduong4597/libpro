#include "borrowedbook.h"

BorrowedBook::BorrowedBook()
{

}

BorrowedBook::BorrowedBook(int BorID, int BoID, int UsID, QDateTime ExDa,QDateTime borrowedDate,bool status)
{
    _BorrowedID=BorID;
    _BookID=BoID;
    _UserID=UsID;
    _ExpiryDate=ExDa;
    _BorrowedDate=borrowedDate;
    _status=status;
}

BorrowedBook::BorrowedBook(QString data)
{
    QStringList values=data.split('|');
    _BorrowedID=((QString)values.at(0)).toInt();
    _BookID=((QString)values.at(1)).toInt();
    _UserID=((QString)values.at(2)).toInt();
    _BorrowedDate=QDateTime::fromString((QString)values.at(3),Qt::ISODate);
   _ExpiryDate=QDateTime::fromString((QString)values.at(4),Qt::ISODate);
   _status=((QString)values.at(5)).toInt()!=0;

}

int BorrowedBook::getBorrowedID()
{
    return _BorrowedID;
}

void BorrowedBook::setBorrowedID(int val)
{
       _BorrowedID=val;
}
int BorrowedBook::getBookID()
{
    return _BookID;
}
void BorrowedBook::setBookID(int val)
{
    _BookID=val;
}


int BorrowedBook::getUserID()
{
    return _UserID;
}
void BorrowedBook::setUserID(int val)
{
    _UserID=val;
}

QDateTime BorrowedBook::getBorrowedDate()
{
    return _BorrowedDate;
}

QDateTime BorrowedBook::getExpiryDate()
{
    return _ExpiryDate;
}

void BorrowedBook::setExpiryDate(QDateTime val)
{
    _ExpiryDate=val;
}

bool BorrowedBook::getStatus()
{
    return _status;
}

void BorrowedBook::setStatus(bool val)
{
    _status=val;
}

QString BorrowedBook::toString()
{
    QString result;
    QTextStream ts(&result);
    ts.setCodec("UTF-8");

    ts << _BorrowedID <<'|' <<_BookID << '|' << _UserID <<'|'<<_BorrowedDate.toString(Qt::ISODate) << '|' <<_ExpiryDate.toString(Qt::ISODate) <<'|' << _status;
    return result;
}
