#ifndef BOOKCART_H
#define BOOKCART_H
#include <QString>
#include <QTextStream>
#include <QList>
#include "book.h"
class BookCart
{
private:
    QList<Book*> _books;
public:
    BookCart();
    void addBook(Book* b);
    QList<Book*> getAllBook();
    void removeBook(Book* b);
    void removeBook(int index);
    void clear();
};

#endif // BOOKCART_H
