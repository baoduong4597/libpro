#ifndef BOOKCARTDETAIL_H
#define BOOKCARTDETAIL_H
#include <QString>
#include <QTextStream>


class BookCartDetail
{
private:
    int _CartDetailID;
    int _CartID;
    int _BookID;
public:
    BookCartDetail();
    BookCartDetail(int CaDe,int Cart, int book);
    BookCartDetail(QString data);
    int getCartDetailld();
    void setCartDetailld(int val);
    int getCartld();
    void setCartld(int val);
    int getBookID();
    void setBookld(int val);
    QString toString();
};

#endif // BOOKCARTDETAIL_H
