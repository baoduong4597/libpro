#include "bookdetail.h"

BookDetail::BookDetail()
{

}

BookDetail::BookDetail(int book, int quan, QString pos, int BoCount)
{
    _BookID=book;
    _Quantity=quan;
    _Position=pos;
    _BorrowedCount=BoCount;
}

BookDetail::BookDetail(QString data)
{
    QStringList values=data.split('|');
    _BookID=((QString)values.at(0)).toInt();
    _Quantity=((QString)values.at(1)).toInt();
    _Position=(QString)values.at(2);
    _BorrowedCount=((QString)values.at(3)).toInt();
}

int BookDetail::getBookID()
{
    return _BookID;
}

void BookDetail::setBookID(int val)
{
    _BookID=val;
}

int BookDetail::getQuantity()
{
    return _Quantity;
}

void BookDetail::setQuantity(int val)
{
    _Quantity=val;
}

QString BookDetail::getPosition()
{
    return _Position;
}

void BookDetail::setPosition(QString val)
{
    _Position=val;
}

int BookDetail::getBorrowedCount()
{
    return _BorrowedCount;
}

void BookDetail::setBorrowedCount(int val)
{
    _BorrowedCount=val;
}

QString BookDetail::toString()
{
    QString result;
    QTextStream ts(&result);
    ts.setCodec("UTF-8");

    ts << _BookID << "|" << _Quantity << '|'<< _Position << '|'<<_BorrowedCount;
    return result;

}
